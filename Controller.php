<?

public function calculate(Request $request)
    {

        //Request the input
        $dist1 = $request->destination1;
        $dist2 = $request->destination2;
        $dist3 = $request->destination3;

        //convert to lower case
        $dest1 = strtolower($dist1);
        $dest2 = strtolower($dist2);
        $dest3 = strtolower($dist3);

        //remove space
        $destA = str_replace(' ', '', $dest1);
        $destB = str_replace(' ', '', $dest2);
        $destC = str_replace(' ', '', $dest3);

        //concatenate 2 destination with diffence possible combination
        $destination1 = $destA . $destB;
        $destination2 = $destB . $destA;
        $destination3 = $destB . $destC;
        $destination4 = $destC . $destB;
        $destination5 = $destC . $destA;
        $destination6 = $destA . $destC;
        // return $destination1;

        //Read  Each line in array formate from the file
        $lines = explode("\n", file_get_contents('/home/kaushal/Desktop/repository/input.txt'));
        foreach ($lines as $line) {

            //Clean up the Array
            $string = str_replace(' ', '', $line); //Remove Space
            $string = str_replace('=', '', $string); // remove "="
            $string = str_replace('to', '', $string); // remove "To"
            $string = preg_replace('/[0-9]+/', '', $string); //Remove number

            //conver to lower case
            $string = strtolower($string);

            if ($string == $destination1) {

                $possibleA = $line;
            }

            if ($string == $destination2) {
                $possibleB = $line;
            }
            if ($string == $destination3) {
                $possibleC = $line;
                $nowurastring = json_encode($line);
                $string = str_replace(' ', '', $nowurastring); //Remove Space
                preg_match_all('!\d+!', $string, $matches);
                $x = json_encode($matches);
                $y = str_replace('"', '', $x);
                $y = str_replace('[', '', $y);
                $y = str_replace(']', '', $y);
                echo $y;
            }


            if ($string == $destination4) {
                $possibleD = $line;
                $nowurastring = json_encode($line);
                $string = str_replace(' ', '', $nowurastring); //Remove Space
                preg_match_all('!\d+!', $string, $matches);
                $x = json_encode($matches);
                $y = str_replace('"', '', $x);
                $y = str_replace('[', '', $y);
                $y = str_replace(']', '', $y);
                echo $y;
            }
            if ($string == $destination5) {

                $possibleE = $line;
                $nowurastring = json_encode($line);
                $string = str_replace(' ', '', $nowurastring); //Remove Space
                preg_match_all('!\d+!', $string, $matches);
                $x = json_encode($matches);
                $y = str_replace('"', '', $x);
                $y = str_replace('[', '', $y);
                $y = str_replace(']', '', $y);
                echo $y;
            }
            if ($string == $destination6) {
                $possibleF = $line;
                $nowurastring = json_encode($line);
                $string = str_replace(' ', '', $nowurastring); //Remove Space
                preg_match_all('!\d+!', $string, $matches);
                $x = json_encode($matches);
                $y = str_replace('"', '', $x);
                $y = str_replace('[', '', $y);
                $y = str_replace(']', '', $y);
                echo $y;
            }

            //match all the found possibilites
            echo $possibleA.$possibleB.$possibleC.$possibleD.$possibleE.$possibleF;
        }